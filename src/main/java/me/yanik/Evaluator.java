package me.yanik;

import me.yanik.Exceptions.DivisionByZeroException;
import me.yanik.Exceptions.InvalidStringException;
import me.yanik.Exceptions.NonBinaryExpressionException;
import me.yanik.Exceptions.NonMatchingParenthesisException;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

/**
 * The Evaluator class is used to evaluate mathematical expressions using
 * Java's widely used data structures.
 */
public class Evaluator {
    /**
     * Evaluates a mathematical expression.
     *
     * Takes in an infix expression and turns into a postfix expression.
     * Then evaluates the postfix expression.
     *
     * @param expression The infix mathematical expression to evaluate
     * @return The result of the expression
     * @throws NonMatchingParenthesisException Exception is thrown if there is a non matching parenthesis
     * @throws InvalidStringException Exception is thrown if there is an invalid string in the expression
     * @throws NonBinaryExpressionException Exception is thrown if there are missing operands or operators.
     * @throws DivisionByZeroException Exception is thrown if there is a division by zero
     */
    public double evaluate(Queue<String> expression) throws NonMatchingParenthesisException,
            InvalidStringException, NonBinaryExpressionException, DivisionByZeroException {
        Queue<String> postfix = toPostfix(expression);
        return evaluatePostfix(postfix);

    }

    /**
     * Transforms an infix expression into a postfix expression
     *
     * The most important part of the algorithm is when it decides whether the stack needs be popped
     * or not before pushing the operator that was de-queued from the infix expression into the operator stack.
     * In order to understand this part, it is necessary to explain when the stack is popped and when it is not popped
     *
     *          Note, by popping the topmost operator from the operators stack and queuing it into the final postfix
     *          queue before pushing the de-queued operator into the operators stack, we're basically specifying
     *          that the popped operator needs to be evaluated before the de-queued operator.
     *
     *      The stack should be popped if:
     *           The operator in the stack has an equal or higher precedence than the de-queued operator
     *
     *               Explanation: Assume operator Nº1 is followed by operator Nº2
     *                            (i.e. operand, operator Nº1, operand, operator Nº2 , operand).
     *                            Operator Nº1, can be evaluated right away only if (1) it has a
     *                            higher precedence than operator Nº2 or (2) it has the same precedence
     *                            as operator Nº2.
     *
     *      The stack should not be popped if:
     *           The operator stack is empty
     *
     *               Explanation: The operator stack is empty, therefore we can't pop anything from the stack
     *
     *           The operator in the stack has a lower precedence than the de-queued operator.
     *
     *               Explanation: Assume operator Nº1 is followed by operator Nº2
     *                            (i.e. operand, operator Nº1, operand, operator Nº2 , operand).
     *                            Operator Nº1 can't be evaluated before operator Nº2
     *                            if operator Nº1 has a lower precedence than operator Nº2.
     *
     *           The operator in the stack is a "("
     *
     *               Explanation: Even though the "(" has the highest precedence, it is not actually actually
     *                            an operand. It should stay in the stack as a marker that will specify what
     *                            operators will be popped when reaching a "(" (normally operators with
     *                            higher precedence need to popped from the stack)
     *
     *           The de-queued operator is a "("
     *
     *               Explanation: Assume the following expression:
     *                               operand, operator Nº1, "(", operand, operator, operand, ")"
     *                            We can't evaluate operator Nº1 before determining the result of
     *                            the expression inside the parentheses, which is why the "(" should simply
     *                            be pushed into the stack.
     *
     *
     * @param infix The infix expression to transform
     * @return Returns a postfix expression
     * @throws NonMatchingParenthesisException Exception is thrown if there is a non matching parenthesis
     * @throws InvalidStringException Exception is thrown if there is an invalid string in the expression
     */
    private Queue<String> toPostfix(Queue<String> infix) throws NonMatchingParenthesisException, InvalidStringException {
        Queue<String> postfix = new ArrayDeque<String>();
        Deque<String> operators = new ArrayDeque<String>();

        while (!infix.isEmpty()) {
            String dequeued = infix.poll();

            if (isOperand(dequeued)) {
                postfix.offer(dequeued);
            } else if (isOperator(dequeued)) {
                if (dequeued.equals(")")) {
                    // If the operator is ")", we need to pop everything from the operators stack
                    // until we reach a "(". The following loop will exit when a "(" is found
                    while (true) {
                        if (operators.isEmpty()){
                            // If still in while loop and the operators stack is empty,
                            // it means that there is no matching "(" (because loop exits when a "(" is found)
                            throw new NonMatchingParenthesisException("Missing '('");
                        }
                        String operator;
                        if ((operator = operators.pop()).equals("(")) {
                            break;
                        } else {
                            postfix.offer(operator);
                        }
                    }
                } else { // Else is reached if operator is not ")"
                    // Deciding if the stack should be popped or not before pushing the de-queued operator into the stack.
                    // There is a while loop since the following boolean expression needs to be false
                    // before pushing the de-queued operator into the stack. (for simplicity's sake, 'dequeued.equals("(")'
                    // is kept inside the boolean expression of the while loop)
                    while (!(operators.isEmpty() || isLowerPrecedence(operators.peek(), dequeued)
                            || operators.peek().equals("(") || dequeued.equals("("))) {
                        postfix.offer(operators.pop());
                    }

                    operators.push(dequeued);
                }
            } else { // Else block reached if isOperand(dequeued) and isOperator(dequeued) return false
                throw new InvalidStringException("Invalid string: " + dequeued);
            }


        }

        // Popping the whole operators stack if it is not empty after the expression queue
        // has been depleted
        while (!operators.isEmpty()) {
            String operator = operators.pop();
            if (operator.equals("(")) {
                // If there is a "(" in the operators stack after the whole expression queue
                // has been depleted, it means that there was no matching ")"
                throw new NonMatchingParenthesisException("Missing ')'");
            }
            postfix.offer(operator);
        }

        return postfix;
    }

    /**
     * Evaluates a postfix expression
     *
     * @param postfix The postfix expression to evaluate
     * @return The result of the expression
     * @throws NonBinaryExpressionException Exception is thrown if there are missing operands or operators.
     * @throws DivisionByZeroException Exception is thrown if there is a division by zero
     */
    private double evaluatePostfix(Queue<String> postfix) throws NonBinaryExpressionException, DivisionByZeroException {
        Deque<Double> operands = new ArrayDeque<Double>();

        while (!postfix.isEmpty()) {
            String dequeued = postfix.poll();

            if (isOperand(dequeued)) {
                operands.push(Double.parseDouble(dequeued));
            } else {
                if (operands.size() < 2) {
                    throw new NonBinaryExpressionException("Missing operand or redundant operator");
                }

                Double operand2 = operands.pop();
                Double operand1 = operands.pop();

                Double operationResult;

                switch (dequeued) {
                    case "+":
                        operationResult = operand1 + operand2;
                        break;
                    case "-":
                        operationResult = operand1 - operand2;
                        break;
                    case "*":
                        operationResult = operand1 * operand2;
                        break;
                    case "/":
                        if (operand2 == 0) {
                            throw new DivisionByZeroException("Dividing by 0");
                        }
                        operationResult = operand1 / operand2;
                        break;
                    default:
                        // default should not be reached if toPostfix() works properly
                        throw new RuntimeException("Unexpected operand in postfix evaluation: " + dequeued);
                }

                operands.push(operationResult);
            }
        }

        if (operands.size() != 1) {
            throw new NonBinaryExpressionException("Missing operator or redundant operand");
        }

        return operands.pop();
    }

    /**
     * Verifies if the element is an operand
     *
     * @param element The element to verify
     * @return True if the element is a operand
     */
    private boolean isOperand(String element) {
        // The following regex matches any numerical string
        return element.matches("-?\\d*[\\.]?\\d+");
    }

    /**
     * Verifies if the element is an operator
     *
     * @param element The element to verify
     * @return True if the element is an operator
     */
    private boolean isOperator(String element) {
        return element.matches("[()\\/*+-]");
    }

    /**
     * Checks if the first operator is of lower precedence than the second operator
     *
     * @param operator1 The first operator
     * @param operator2 The second operator
     * @return True if the first operator has a lower precedence than the second operator
     */
    private boolean isLowerPrecedence(String operator1, String operator2) {
        return precedenceLevelOf(operator1) < precedenceLevelOf(operator2);
    }

    /**
     * Returns the precedence level of the operator. The higher the precedence
     * of the operator, the higher is the returned number.
     *
     * @param operator The precedence of this operator will be returned
     * @return The precedence level of the operator
     */
    private int precedenceLevelOf(String operator) {
        switch (operator) {
            case "+":
            case "-":
                return 0;
            case "*":
            case "/":
                return 1;
            case "(":
            case ")":
                return 2;
            default:
                // default should not be called if operator was validated correctly
                throw new RuntimeException("Unexpected operator: " + operator);
        }
    }
}
