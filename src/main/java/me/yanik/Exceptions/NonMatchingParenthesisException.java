package me.yanik.Exceptions;

public class NonMatchingParenthesisException extends Exception {
    public NonMatchingParenthesisException() {
        super();
    }

    public NonMatchingParenthesisException(String message) {
        super(message);
    }
}
