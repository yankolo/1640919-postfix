package me.yanik.Exceptions;

public class NonBinaryExpressionException extends Exception {
    public NonBinaryExpressionException() {
        super();
    }

    public NonBinaryExpressionException(String message) {
        super(message);
    }
}
