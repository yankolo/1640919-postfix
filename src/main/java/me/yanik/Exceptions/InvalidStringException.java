package me.yanik.Exceptions;

public class InvalidStringException extends Exception {
    public InvalidStringException() {
        super();
    }

    public InvalidStringException(String message) {
        super(message);
    }
}
