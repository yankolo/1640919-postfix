package test.java.me.yanik;

import me.yanik.Evaluator;
import me.yanik.Exceptions.DivisionByZeroException;
import me.yanik.Exceptions.InvalidStringException;
import me.yanik.Exceptions.NonBinaryExpressionException;
import me.yanik.Exceptions.NonMatchingParenthesisException;
import org.junit.*;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;


@RunWith(Enclosed.class)
public class EvaluatorTests {

    /* ----------- Parameterized Tests Cases - Valid Expressions ------------ */
    @RunWith(Parameterized.class)
    public static class ValidExpressionTests {
        private Queue<String> infix;
        private double expectedResult;

        public ValidExpressionTests(Queue<String> infix, double expectedResult) {
            this.infix = infix;
            this.expectedResult = expectedResult;
        }

        @Parameterized.Parameters(name = "{index}: expression={0} result={1}")
        public static Collection<Object> data() {
            return Arrays.asList(new Object[][]{
                    {new ArrayDeque<String>(Arrays.asList("7", "+", "15")), 22.0},
                    {new ArrayDeque<String>(Arrays.asList("18", "+", "16", "*", "6")), 114.0},
                    {new ArrayDeque<String>(Arrays.asList("17", "+", "5", "*", "17", "+", "10")), 112.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "8", "+", "3", ")", "*", "(", "4", "+", "15", ")")), 209.0},
                    {new ArrayDeque<String>(Arrays.asList("14", "*", "11", "+", "9", "*", "8")), 226.0},
                    {new ArrayDeque<String>(Arrays.asList("10", "+", "20", "+", "8", "+", "7")), 45.0},
                    {new ArrayDeque<String>(Arrays.asList("10", "+", "3", "*", "5", "/", "(", "16", "-", "4", ")")), 11.25},
                    {new ArrayDeque<String>(Arrays.asList("5", "*", "(", "3", "+", "7", "/", "4", ")")), 23.75},
                    {new ArrayDeque<String>(Arrays.asList("19", "*", "(", "12", "+", "8", ")", "/", "1")), 380.0},
                    {new ArrayDeque<String>(Arrays.asList("14", "*", "1", "+", "5", "/", "1")), 19.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "5", "+", "(", "2", "*", "3", ")", ")")), 11.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "7", "+", "(", "4", "+", "10", ")", ")")), 21.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "30", "+", "(", "10", "*", "20", ")", ")", "/", "(", "11", "-", "3", ")")), 28.75},
                    {new ArrayDeque<String>(Arrays.asList("(", "(", "15", "+", "10", ")", "*", "(", "20", "+", "14", ")", ")")), 850.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "4", "*", "(", "15", "*", "(", "(", "(", "6", "+", "18", ")", "+", "1", ")", "*", "15", ")", ")", ")")), 22500.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "(", "3", "*", "(", "(", "(", "(", "14", "+", "(", "(", "11", "+", "17", ")", "*", "1", ")", ")", "*", "12", ")", "*", "2", ")", "*", "10", ")", ")", "+", "8", ")")), 30248.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "7", "+", "2", ")", "*", "4")), 36.0},
                    {new ArrayDeque<String>(Arrays.asList("1", "+", "2", "+", "3")), 6.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "(", "18", "+", "12", ")", "-", "3", "*", "(", "1", "/", "20", ")", ")", "+", "12")), 41.85},
                    {new ArrayDeque<String>(Arrays.asList("80.3", "-", "30", "/", "2", "+", "3", "*", "2")), 71.3},
                    {new ArrayDeque<String>(Arrays.asList("(", "-0.4", "/", "(", "2", "+", "30", ")", "-", "3", ")")), -3.0125},
                    {new ArrayDeque<String>(Arrays.asList("3", "+", "(", "1", "+", "(", "2", "+", "3", ")", ")", "*", "2")), 15.0},
                    {new ArrayDeque<String>(Arrays.asList("(", "63", "+", "2", ")", "/", "2", "*", "2", "+", "3")), 68.0},
                    {new ArrayDeque<String>(Arrays.asList("100", "+", "2", "*", "2", "*", "6", "-", "50")), 74.0},
                    {new ArrayDeque<String>(Arrays.asList("49", "+", "4", "+", "2", "+", "3", "+", "5", "-", "3", "/", "2")), 61.5},
            });
        }

        @Test
        public void evaluate_ValidExpression_CorrectResult() throws NonMatchingParenthesisException,
                InvalidStringException, NonBinaryExpressionException, DivisionByZeroException {
            Evaluator evaluator = new Evaluator();
            double actualResult = evaluator.evaluate(this.infix);

            Assert.assertEquals(expectedResult, actualResult, 0.0001);
        }
    }

    /* ----------- Parameterized Tests Cases - Invalid Strings ------------ */
    @RunWith(Parameterized.class)
    public static class InvalidStringsTests {
        private Queue<String> infix;

        public InvalidStringsTests(Queue<String> infix) {
            this.infix = infix;
        }

        @Parameterized.Parameters(name = "{index}: expression={0} result=InvalidStringException")
        public static Collection<Object> data() {
            return Arrays.asList(new Object[]{
                    new ArrayDeque<String>(Arrays.asList("&", "+", "15")),
                    new ArrayDeque<String>(Arrays.asList("A", "+", "16", "*", "6")),
                    new ArrayDeque<String>(Arrays.asList("17", "+", "E", "*", "b", "+", "10")),
                    new ArrayDeque<String>(Arrays.asList("(", "8", "+", "3", "}", "*", "(", "4", "+", "15", ")")),
                    new ArrayDeque<String>(Arrays.asList("14", "*", "11", "+", "9", "**", "8"))
            });
        }

        @Test(expected=InvalidStringException.class)
        public void evaluate_InvalidExpression_InvalidStringException() throws NonMatchingParenthesisException,
                InvalidStringException, NonBinaryExpressionException, DivisionByZeroException {
            Evaluator evaluator = new Evaluator();
            evaluator.evaluate(this.infix);

            Assert.fail();
        }
    }

    /* ----------- Parameterized Tests Cases - Non Matching Parenthesis ------------ */
    @RunWith(Parameterized.class)
    public static class NonMatchingParenthesisTests {
        private Queue<String> infix;

        public NonMatchingParenthesisTests(Queue<String> infix) {
            this.infix = infix;
        }

        @Parameterized.Parameters(name = "{index}: expression={0} result=NonMatchingParenthesisException")
        public static Collection<Object> data() {
            return Arrays.asList(new Object[]{
                    new ArrayDeque<String>(Arrays.asList("(", "8", "+", "3", ")", "*", "4", "+", "15", ")")),
                    new ArrayDeque<String>(Arrays.asList("5", "*", "3", "+", "7", "/", "4", ")")),
                    new ArrayDeque<String>(Arrays.asList("19", "*", "(", "12", "+", "8", "/", "1")),
                    new ArrayDeque<String>(Arrays.asList("(", "(", "3", "*", "(", "(", "(", "14", "(", "(", "11", "+", "17", ")", "*", "1", ")", ")", "*", "12", ")", "*", "2", ")", "*", "10", ")", ")", "+", "8", ")")),
                    new ArrayDeque<String>(Arrays.asList("(", "7", "+", "2", "*", "4")),

            });
        }

        @Test(expected=NonMatchingParenthesisException.class)
        public void evaluate_InvalidExpression_NonMatchingParenthesisException() throws NonMatchingParenthesisException,
                InvalidStringException, NonBinaryExpressionException, DivisionByZeroException {
            Evaluator evaluator = new Evaluator();
            evaluator.evaluate(this.infix);

            Assert.fail();
        }
    }

    /* ----------- Parameterized Tests Cases - Non Binary Expression ------------ */
    @RunWith(Parameterized.class)
    public static class NonBinaryExpressionTests {
        private Queue<String> infix;

        public NonBinaryExpressionTests(Queue<String> infix) {
            this.infix = infix;
        }

        @Parameterized.Parameters(name = "{index}: expression={0} result=NonBinaryExpressionException")
        public static Collection<Object> data() {
            return Arrays.asList(new Object[]{
                    new ArrayDeque<String>(Arrays.asList("7", "+", "+", "15")),
                    new ArrayDeque<String>(Arrays.asList("18", "+",  "*", "6")),
                    new ArrayDeque<String>(Arrays.asList("17", "+", "5", "*", "17", "+")),
                    new ArrayDeque<String>(Arrays.asList("(", "8", "+", "+", "+", "3", ")", "*", "(", "4", "+", "15", ")")),
                    new ArrayDeque<String>(Arrays.asList("14", "*", "+", "9", "*", "8"))
            });
        }

        @Test(expected=NonBinaryExpressionException.class)
        public void evaluate_InvalidExpression_NonBinaryExpressionException() throws NonMatchingParenthesisException,
                InvalidStringException, NonBinaryExpressionException, DivisionByZeroException {
            Evaluator evaluator = new Evaluator();
            evaluator.evaluate(this.infix);

            Assert.fail();
        }
    }

    /* ----------- Parameterized Tests Cases - Division By Zero------------ */
    @RunWith(Parameterized.class)
    public static class DivisionByZeroTests {
        private Queue<String> infix;

        public DivisionByZeroTests(Queue<String> infix) {
            this.infix = infix;
        }

        @Parameterized.Parameters(name = "{index}: expression={0} result=DivisionByZeroException")
        public static Collection<Object> data() {
            return Arrays.asList(new Object[]{
                    new ArrayDeque<String>(Arrays.asList("10", "+", "3", "*", "5", "/", "(", "16", "-", "16", ")")),
                    new ArrayDeque<String>(Arrays.asList("(", "(", "18", "+", "12", ")", "-", "3", "*", "(", "1", "/", "0", ")", ")", "+", "12")),
                    new ArrayDeque<String>(Arrays.asList("80.3", "-", "30", "/", "0", "+", "3", "*", "2")),
                    new ArrayDeque<String>(Arrays.asList("10", "+", "3", "*", "5", "/", "0")),
                    new ArrayDeque<String>(Arrays.asList("5", "*", "(", "3", "+", "7", "/", "0", ")")),
            });
        }

        @Test(expected=DivisionByZeroException.class)
        public void evaluate_InvalidExpression_DivisionByZeroException() throws NonMatchingParenthesisException,
                InvalidStringException, NonBinaryExpressionException, DivisionByZeroException {
            Evaluator evaluator = new Evaluator();
            evaluator.evaluate(this.infix);

            Assert.fail();
        }
    }

}
